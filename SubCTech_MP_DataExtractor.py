import csv, sys, glob

################## Global variables ###################

app_data = {"file_list":[""],"current_file_processing":0,"file_nb":0}
previous_row_data = {"date":{"year":int("0000"),"month":int(00),"day":int(00)},"time":{"hour":int(00),"minute":int(00),"second":int(00)},"position":{"latitude":float(0.0),"longitude":float(0.0)},"filter_data":{"volume":float(0.0),"debit":float(0.0),"filter_id":int(-1)}}

####################### Function ######################

def parseArgs(args):
    print("############ MP data extractor #############")
    for i in range(1,len(args)):
        match args[i]:
            case "-h" | "--help":
                i+=1
                print("#################### HELP ##################")
                print("USAGE :")
                print(" - Process file.log : ")
                print("     python .\extract.py path\\to\\files.log")
                print(" - Process all log files in folder :")
                print("     python .\extract.py path\\to\\files\\*.log")
                print("############################################")
                sys.exit(0)
            case _:
                if i < (len(args)-1):
                    raise NameError("Unknown option")
    app_data["file_list"] = glob.glob(args[i])
    if len(app_data["file_list"]) == 0:
        raise NameError("Invalid file name "+ "\""+str(args[i])+"\"")
    else:
        app_data["file_nb"] = len(app_data["file_list"])
        print("######## "+str(app_data["file_nb"])+" input files to process #########")

#######################################################


parseArgs(sys.argv)

with open("decode_output_MP.csv","w") as output_file:
    output_file.write("Day,Month,Year,Hour,Minute,Seconde,Latitude,Longitude,filter_ID,Volume,Flow Rate\r")

for file in app_data["file_list"]:
    with open(file) as csv_file:
        app_data["current_file_processing"] += 1
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if row[0] == "@DATA":
                raw_date = row[1].split("-")
                date = {"year":int(raw_date[0]),"month":int(raw_date[1]),"day":int(raw_date[2])}
                raw_time = row[2].split(":")
                time = {"hour":int(raw_time[0]),"minute":int(raw_time[1]),"second":int(raw_time[2])}
                if row[9] == "/" or row[10] == "/":
                    position = {"latitude":float("0.0"),"longitude":float("0.0")}
                else:
                    position = {"latitude":float(row[9]),"longitude":float(row[10])}
                filter_data = {"volume":float(row[43]),"debit":float(row[44]),"filter_id":int(row[45])}
                #Decide if we save this line or not
                if filter_data["filter_id"] != previous_row_data["filter_data"]["filter_id"]:
                    #print("New filter_id detected, previous = "+str(previous_row_data["filter_data"]["filter_id"])+" new = "+str(filter_data["filter_id"]))
                    print("Processing file "+ str(app_data["current_file_processing"]) +"/"+str(app_data["file_nb"]))
                    with open("decode_output_MP.csv","a") as output_file:
                        if previous_row_data["filter_data"]["filter_id"] != -1:
                            output_file.write(str(previous_row_data["date"]["day"])+","+str(previous_row_data["date"]["month"])+","+str(previous_row_data["date"]["year"])+","+str(previous_row_data["time"]["hour"])+","+str(previous_row_data["time"]["minute"])+","+str(previous_row_data["time"]["second"])+","+str(previous_row_data["position"]["latitude"])+","+str(previous_row_data["position"]["longitude"])+","+str(previous_row_data["filter_data"]["filter_id"])+","+str(previous_row_data["filter_data"]["volume"])+","+str(previous_row_data["filter_data"]["debit"])+"\r")
                        output_file.write(str(date["day"])+","+str(date["month"])+","+str(date["year"])+","+str(time["hour"])+","+str(time["minute"])+","+str(time["second"])+","+str(position["latitude"])+","+str(position["longitude"])+","+str(filter_data["filter_id"])+","+str(filter_data["volume"])+","+str(filter_data["debit"])+"\r")
                #Save data for the next one
                previous_row_data["date"] = date
                previous_row_data["time"] = time
                previous_row_data["position"] = position
                previous_row_data["filter_data"] = filter_data
print("################# COMPLETE #################")

   