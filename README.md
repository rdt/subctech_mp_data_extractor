# SubCTech_MP_Data_Extractor
This script is designed to extract data from SubCtech's MP .log files.
It outputs a .csv file including the following headers :  Day	Month	Year	Hour	Minute	Seconde	Latitude	Longitude	filter_ID	Volume	Flow Rate
## Installation
This script is designed with python. 
Python v3.10 or higher version is required.
It might be downloaded from https://www.python.org/downloads/
### Adding Python to Windows PATH
The complete path of python.exe can be added by:
- Right-clicking This PC and going to Properties.
- Clicking on the Advanced system settings in the menu on the left.
- Clicking on the Environment Variables button o​n the bottom right.
- In the System variables section, selecting the Path variable and clicking on Edit. The next screen will show all the directories that are currently a part of the PATH variable.
- Clicking on New and entering Python’s install directory.
Usually you can find the python installed binary in this path location: C:\Users\<YOURNAME>\AppData\Local\Programs\Python\Python310\

## Usage
- Open the folder containing the SubCTech_MP_DataExtractor.py script
- Hold "Shift" key down and right click
- Click "Open Powershell"
- Type ```python .\extract.py path\to\file.log``` if you want to extract data from a single file
    or ```python .\extract.py path\to\files\*.log``` to extract data from all the files contained in a folder

## Output
The script outputs a .csv file named "decode_output_MP.csv" and located in the same folder as the script.   
Warning : The output file is overwritten each time the script is called.